//
//  main.cpp
//  BS_Ideas
//
//  Created by Marta Cabo Nodar on 06/05/15.
//  Copyright (c) 2015 Marta Cabo. All rights reserved.
//
//This is a first start of a basic Beam Search. Initially this is created to test ideas, not as final version.
//============================================================================================================
#include "classes_BPGC.h"
//#include <mach-o/dyld.h>	/* _NSGetExecutablePath */
//Declaración de funciones necesarias

vector<PIEZA>
read_pieces (const string &name);

bool
orden_area (PIEZA i, PIEZA j);
void
PrintSolution (const char *argv, const char *instance_name, TREE &bs_sol,
	       double runtime);
void
PrintTree (const char *argv, char *instance_name, TREE &bs_sol, double runtime);

#define CLOCKS_PER_MS (CLOCKS_PER_SEC / 1000)

int
main (int argc, const char * argv[])
{
  int k = 2;
//
//    char path[1024];
//    uint32_t size = sizeof(path);
//    if (_NSGetExecutablePath(path, &size) == 0)
//        printf("executable path is %s\n", path);
//    else
//        printf("buffer too small; need size %u\n", size);
//
  ofstream fp;

  fp.open ("Results.txt", ios_base::app);
  if (!fp.is_open ())
    {
      cout << "ERROR: Unable to open Results file\n";
      exit (1);
    }

  for (k = 2; k < argc; k++)
    {
      // For all instances.

      // Reads instance name
      string instance_name (argv[k]);

      //Read pieces
      vector<PIEZA> item;
      item = read_pieces (instance_name);
      // ============================
      // Order pieces
      //By area
      sort (item.begin (), item.end (), orden_area);
      //Calculate the total area of the items:
      double AreaPz = 0;
      for (int i = 0; i < item.size (); i++)
	AreaPz = AreaPz + item[i].getArea ();
      // ============================

      TREE bs_sol;
      clock_t start = clock ();
      bs_sol.build_solution (argv[1], item);
      clock_t finish = clock ();
      double runtime = (finish - start) / CLOCKS_PER_MS; //Running time in milliseconds.
      runtime = runtime / 1000; //Running time in seconds, without loosing precission.

      //Print solution
      //====================================================
//      strcpy (instance_name, argv[k]);
//      PrintSolution (argv[1], instance_name, bs_sol, runtime);

      PrintSolution (argv[1], argv[k], bs_sol, runtime);

//      PrintTree (argv[1], instance_name, bs_sol, runtime);
      //====================================================

      //Identify solution node
      //==============================
      list<NODE>::iterator last_node;
      list<NODE> tree = bs_sol.get_tree ();
      last_node = tree.end ();
      last_node--;
      //Calculate area of the bin
      double AreaBin = last_node->getL () * last_node->getW ();
      int last_level = last_node->get_level ();
      double Residual = GRANDE;
      //When OF is Fractional Number of bins, we want the branch with less utilization on the last bin.
      //=====================================================================================
      int bestID = -1;
      while (last_node->get_level () == last_level)
	{
	  if (last_node->get_IDdisp ().empty ())
	    {
	      if (Residual > last_node->getPropUtil ())
		{
		  Residual = last_node->getPropUtil ();
		  bestID = last_node->getID ();
		}
	    }
	  last_node--;
	}
      //====================================================================
      //When OF is %Utilization we want the branch with more overall %utilization
//        int bestID=-1;
//        while(last_node->get_level() == last_level)
//        {
//            if(last_node->get_IDdisp().empty() && last_node->get_IDdisp().empty())
//            {
//                NODE child;
//                child = *last_node;
//                double usage = 0;
//                while(child.get_Pred() != NULL)//While not at FATHER level
//                {
//                    usage =usage + child.getPropUtil();
//                    child = *child.get_Pred();
//                }
//                if(MoreUtil < usage)
//                {
//                    MoreUtil = usage;
//                    bestID = last_node->getID();
//                }
//            }
//            last_node--;
//        }
      //====================================================================

      //Retrieve best solution
      //======================
      last_node = tree.end ();
      last_node--;
      double Utilization = 0;
      while (last_node->get_level () == last_level)
	{
	  if (last_node->get_IDdisp ().empty ()
	      && last_node->getID () == bestID)
	    {
	      Residual = last_node->getPropUtil ();
//                NODE child;
//                child = *last_node;
//                while(child.get_Pred() != NULL)//While not at FATHER level
//                {
//                    Utilization =Utilization + child.getPropUtil();
//                    child = *child.get_Pred();
//                }

	    }
	  last_node--;
	}
      //Total % Utilization: Calculated as in the Omega Paper:
      Utilization = AreaPz / ((last_level - 1 + Residual) * AreaBin);
      double Frac_N_Bins = last_level - 1 + Residual;
      cout << "Instance: " << instance_name << "\n";
      cout << "Number of bins: " << last_level << "\n";
      cout << "Fractional No of Bins: " << Frac_N_Bins << "\n";
      cout << "%Utilization: " << Utilization << "\n";
      cout << "Running time " << runtime << "\n";
      fp << instance_name << "\t" << last_level << "\t" << Utilization << "\t"
	  << Frac_N_Bins << "\t" << runtime << "\n";
    }
  return 0;
}
